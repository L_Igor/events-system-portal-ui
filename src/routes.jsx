import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './components/Layout';
import Login from './components/Login';
import Logout from './components/Logout';

import UsersListContainer from './containers/UsersListContainer';
import UserCreateContainer from './containers/UserCreateContainer';
import UserDetailsContainer from './containers/UserDetailsContainer';
import UserEditContainer from './containers/UserEditContainer';

import LecturesListContainer from './containers/LecturesListContainer';
import LectureCreateContainer from './containers/LectureCreateContainer';
import LectureDetails from './components/LectureDetails';
import LectureEditContainer from './containers/LectureEditContainer';
import NotFound from './components/NotFound';


const Users = function ({ match }) {
    return (
        <Switch>
            <Route exact path={`${match.path}`} component={UsersListContainer} />
            <Route exact path={`${match.path}/new`} component={UserCreateContainer} />
            <Route exact path={`${match.path}/:id`} component={UserDetailsContainer} />
            <Route exact path={`${match.path}/:id/edit`} component={UserEditContainer} />
        </Switch>
    );
}

const Lectures = function ({ match }) {
    return (
        <Switch>
            <Route exact path={`${match.path}`} component={LecturesListContainer} />
            <Route exact path={`${match.path}/new`} component={LectureCreateContainer} />
            <Route exact path={`${match.path}/:id`} component={LectureDetails} />
            <Route exact path={`${match.path}/:id/edit`} component={LectureEditContainer} />
        </Switch>
    );
}

export const routes =
    <Layout>
        <Switch>
            <Route exact path='/' component={Users} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/logout' component={Logout} />
            <Route path='/users' component={Users} />
            <Route path='/lectures' component={Lectures} />
            <Route component={NotFound} />
        </Switch>
    </Layout>;
