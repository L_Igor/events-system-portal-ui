import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as lecturesActions from '../store/lectures/actions';
import * as usersSelectors from '../store/users/reducer';

import LectureCreateComponent from '../components/LectureCreateComponent';
import * as usersActions from "../store/users/actions";

class LectureCreateContainer extends Component {
    constructor(props) {
        super(props);

        this.addLecture = this.addLecture.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(usersActions.getUsers());
    }

    render() {
        if (!this.props.usersList) return this.renderLoading();

        return (
            <div>
                <LectureCreateComponent
                    usersList={this.props.usersList}
                    addLecture={this.addLecture}
                    cancel={this.cancel}
                />
            </div>
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    addLecture(values) {
        this.props.dispatch(lecturesActions.addLecture(values));
    }

    cancel() {
        this.props.history.goBack();
    }
}

function mapStateToProps(state) {
    return {
        usersList: usersSelectors.getUsersList(state)
    };
}

export default connect(mapStateToProps)(LectureCreateContainer);