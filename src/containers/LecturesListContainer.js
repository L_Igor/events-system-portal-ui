import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as lecturesActions from '../store/lectures/actions';
import * as lecturesSelectors from '../store/lectures/reducer';
import * as usersActions from '../store/users/actions';

import LecturesListComponent from '../components/LecturesListComponent';
import * as usersSelectors from "../store/users/reducer";

class LecturesListContainer extends Component {
    constructor(props) {
        super(props);

        this.onNewLectureClickHandler = this.onNewLectureClickHandler.bind(this);
        this.onEditClickHandler = this.onEditClickHandler.bind(this);
        this.onDeleteClickHandler = this.onDeleteClickHandler.bind(this);
        this.onUserDetailsClickHandler = this.onUserDetailsClickHandler.bind(this);
        this.onToggleLectureClickHandler = this.onToggleLectureClickHandler.bind(this);
        this.lectureSearchHandler = this.lectureSearchHandler.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(lecturesActions.getLectures());
        this.props.dispatch(usersActions.getUsers());
    }

    render() {
        if (!this.props.lecturesList) return this.renderLoading();

        return (
            <div>
                <LecturesListComponent
                    lecturesList={this.props.lecturesList}
                    newLectureClick={this.onNewLectureClickHandler}
                    editClick={this.onEditClickHandler}
                    deleteClick={this.onDeleteClickHandler}
                    userClick={this.onUserDetailsClickHandler}
                    toggleLectureClick={this.onToggleLectureClickHandler}
                    openLectureId={this.props.openLectureId}
                    searchString={this.props.searchString}
                    lectureSearchHandler={this.lectureSearchHandler}
                />
            </div>
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    onNewLectureClickHandler() {
        this.props.history.push(`${this.props.match.url}/new`);
    }

    onEditClickHandler(id) {
        this.props.history.push(`${this.props.match.url}/${id}/edit`);
    }

    onDeleteClickHandler(id) {
        this.props.dispatch(lecturesActions.deleteLecture(id));
    }

    onUserDetailsClickHandler(id) {
        this.props.history.push(`/users/${id}`);
    }

    onToggleLectureClickHandler(lectureId) {
        if (this.props.openLectureId === lectureId) {
            this.props.dispatch(lecturesActions.closeLecture());
            return;
        }

        this.props.dispatch(lecturesActions.openLecture(lectureId));
    }


    lectureSearchHandler(searchString) {
        this.props.dispatch(lecturesActions.searchLecture(searchString));
    }
}

function mapStateToProps(state) {
    return {
        lecturesList: lecturesSelectors.getLecturesList(state),
        openLectureId: lecturesSelectors.getOpenLectureId(state),
        searchString: lecturesSelectors.getSearchTerm(state)
    };
}

export default connect(mapStateToProps)(LecturesListContainer);