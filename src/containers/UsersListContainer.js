import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as usersActions from '../store/users/actions';
import * as usersSelectors from '../store/users/reducer';

import UsersListComponent from '../components/UsersListComponent';

class UsersListContainer extends Component {
    constructor(props) {
        super(props);

        this.onNewUserClickHandler = this.onNewUserClickHandler.bind(this);
        this.onEditClickHandler = this.onEditClickHandler.bind(this);
        this.onDeleteClickHandler = this.onDeleteClickHandler.bind(this);
        this.onUserDetailsClickHandler = this.onUserDetailsClickHandler.bind(this);
        this.onToggleUserClickHandler = this.onToggleUserClickHandler.bind(this);
        this.userSearchHandler = this.userSearchHandler.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(usersActions.getUsers());
    }

    render() {
        if (!this.props.usersList) return this.renderLoading();
        return (
            <div>
                <UsersListComponent
                    usersList={this.props.usersList}
                    newUser={this.onNewUserClickHandler}
                    editClick={this.onEditClickHandler}
                    deleteClick={this.onDeleteClickHandler}
                    userClick={this.onUserDetailsClickHandler}
                    toggleUserClick={this.onToggleUserClickHandler}
                    openUserId={this.props.openUserId}
                    searchString={this.props.searchString}
                    userSearchHandler={this.userSearchHandler}
                />
            </div>
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    onNewUserClickHandler() {
        this.props.history.push(`${this.props.match.url}/new`);
    }

    onEditClickHandler(id){
        this.props.history.push(`${this.props.match.url}/${id}/edit`);
    }

    onDeleteClickHandler(id){
        this.props.dispatch(usersActions.deleteUser(id));
    }

    onUserDetailsClickHandler(id) {
        this.props.history.push(`/users/${id}`);
    }

    onToggleUserClickHandler(UserId) {
        if (this.props.openUserId === UserId) {
            this.props.dispatch(usersActions.closeUser());
            return;
        }

        this.props.dispatch(usersActions.openUser(UserId));
    }

    userSearchHandler(searchString) {
        this.props.dispatch(usersActions.searchUser(searchString));
    }
}

function mapStateToProps(state) {
    return {
        usersList: usersSelectors.getUsersList(state),
        openUserId: usersSelectors.getOpenUserId(state),
        searchString: usersSelectors.getSearchTerm(state)
    };
}

export default connect(mapStateToProps)(UsersListContainer);