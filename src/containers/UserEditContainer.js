import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as usersActions from '../store/users/actions';
import * as usersSelectors from '../store/users/reducer';

import User from '../models/user';

import UserEditComponent from '../components/UserEditComponent';

class UserEditContainer extends Component {
    constructor(props) {
        super(props);

        this.updateUser = this.updateUser.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(usersActions.getUser(this.props.match.params.id));
    }

    render() {
        return (
            <div>
                <UserEditComponent
                    user={this.props.user}
                    updateUser={this.updateUser}
                    cancel={this.cancel}
                />
            </div>
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    updateUser(values) {
        const photo = values.photo && values.photo[0];
        this.props.dispatch(usersActions.updateUser(this.props.match.params.id, values, photo));
    }

    cancel() {
        this.props.history.goBack();
    }
}

function mapStateToProps(state) {
    return {
        user: User.convertFromDtoToVm(usersSelectors.getActiveUser(state)),
    };
}

export default connect(mapStateToProps)(UserEditContainer);