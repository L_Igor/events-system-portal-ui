import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as usersSelectors from '../store/users/reducer';

import UsersCardComponent from '../components/UserCardComponent';

class UsersCardContainer extends Component {
    constructor(props) {
        super(props);

        this.onUserDetailsClickHandler = this.onUserDetailsClickHandler.bind(this);
    }

    render() {
        if (!this.props.user) return this.renderLoading();
        return (
            <UsersCardComponent
                user={this.props.user}
                userClick={this.onUserDetailsClickHandler}
            />
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    onUserDetailsClickHandler(id) {
        this.props.history.push(`/users/${id}`);
    }
}

function mapStateToProps(state, ownProps) {
    return {
        user: usersSelectors.getUserById(state, ownProps.userId)
    };
}

export default withRouter(connect(mapStateToProps)(UsersCardContainer));