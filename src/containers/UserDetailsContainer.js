import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as usersActions from '../store/users/actions';
import * as usersSelectors from '../store/users/reducer';
import * as lecturesActions from '../store/lectures/actions';

import UserDetailsComponent from '../components/UserDetailsComponent';

class UsersDetailsContainer extends Component {
    constructor(props) {
        super(props);

        this.onNewUserClickHandler = this.onNewUserClickHandler.bind(this);
        this.onUserEditClickHandler = this.onUserEditClickHandler.bind(this);
        this.onUserDeleteClickHandler = this.onUserDeleteClickHandler.bind(this);

        this.onLectureEditClickHandler = this.onLectureEditClickHandler.bind(this);
        this.onLectureDeleteClickHandler = this.onLectureDeleteClickHandler.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(usersActions.getUser(this.props.match.params.id));
        this.props.dispatch(usersActions.getUserLectures(this.props.match.params.id));
        this.props.dispatch(usersActions.getUsers());
    }

    render() {
        if (!this.props.user || !this.props.userLectures) return this.renderLoading();
        const gravatarInfo = {
            email: this.props.user.email,
            fullName: this.props.user.fullName,
            photo: this.props.user.photo
        };
        return (
            <UserDetailsComponent
                gravatarInfo ={gravatarInfo}
                user={this.props.user}
                userLectures={this.props.userLectures}
                newUserClick={this.onNewUserClickHandler}
                editUserClick={this.onUserEditClickHandler}
                deleteUserClick={this.onUserDeleteClickHandler}
                editLectureClick={this.onLectureEditClickHandler}
                deleteLectureClick={this.onLectureDeleteClickHandler}
            />
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    onNewUserClickHandler() {
        this.props.history.push(`/users/new`);
    }

    onUserEditClickHandler(id){
        this.props.history.push(`/users/${id}/edit`);
    }

    onUserDeleteClickHandler(id){
        this.props.dispatch(usersActions.deleteUser(id));
    }

    onLectureEditClickHandler(id){
        this.props.history.push(`/lectures/${id}/edit`);
    }

    onLectureDeleteClickHandler(id){
        this.props.dispatch(lecturesActions.deleteLecture(id));
    }
}

function mapStateToProps(state) {
    return {
        user: usersSelectors.getActiveUser(state),
        userLectures: usersSelectors.getUserLectures(state)
    };
}

export default connect(mapStateToProps)(UsersDetailsContainer);