import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as usersActions from '../store/users/actions';
import * as usersSelectors from '../store/users/reducer';

import UserCreateComponent from '../components/UserCreateComponent';

class UserCreateContainer extends Component {
    constructor(props) {
        super(props);

        this.addUser = this.addUser.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    render() {
        return (
            <div>
                <UserCreateComponent
                    addUser={this.addUser}
                    cancel={this.cancel}
                />
            </div>
        );
    }

    addUser(values) {
        const photo = values.photo && values.photo[0];
        this.props.dispatch(usersActions.addUser(values, photo));
    }

    cancel() {
        this.props.history.goBack();
    }
}

function mapStateToProps(state) {
    return {
        usersList: usersSelectors.getUsersList(state)
    };
}

export default connect(mapStateToProps)(UserCreateContainer);