import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as lecturesActions from '../store/lectures/actions';
import * as lecturesSelectors from '../store/lectures/reducer';
import * as usersActions from '../store/users/actions';
import * as usersSelectors from '../store/users/reducer';

import Lecture from '../models/lecture';

import LectureEditComponent from '../components/LectureEditComponent';

class LectureEditContainer extends Component {
    constructor(props) {
        super(props);

        this.updateLecture = this.updateLecture.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(lecturesActions.getLecture(this.props.match.params.id));
        this.props.dispatch(usersActions.getUsers());
    }

    render() {
        if (!this.props.usersList) return this.renderLoading();

        return (
            <div>
                <LectureEditComponent
                    lecture={this.props.lecture}
                    usersList={this.props.usersList}
                    updateLecture={this.updateLecture}
                    cancel={this.cancel}
                />
            </div>
        );
    }

    renderLoading() {
        return (
            <p>Loading...</p>
        );
    }

    updateLecture(values) {
        this.props.dispatch(lecturesActions.updateLecture(this.props.match.params.id, values));
    }

    cancel() {
        this.props.history.goBack();
    }
}

function mapStateToProps(state) {
    return {
        lecture: Lecture.convertFromDtoToVm(lecturesSelectors.getActiveLecture(state)),
        usersList: usersSelectors.getUsersList(state)
    };
}

export default connect(mapStateToProps)(LectureEditContainer);