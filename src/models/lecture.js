import DateConverter from '../helpers/dateConverter';

class Lecture {
    static convertFromVmToDto(lectureVm) {
        if (!lectureVm) {
            return {};
        }

        return {
            name: lectureVm.name,
            description: lectureVm.description,
            startDateTime: DateConverter.toDateTimeString(lectureVm.date, lectureVm.start_time),
            endDateTime: DateConverter.toDateTimeString(lectureVm.date, lectureVm.end_time),
            eventId: 1,
            userId: lectureVm.lector
        };
    }

    static convertFromDtoToVm(lectureDto) {
        if (!lectureDto) {
            return {};
        }

        return {
            name: lectureDto.name,
            description: lectureDto.description,
            date: DateConverter.toDateString(lectureDto.startDateTime),
            start_time: DateConverter.toTimeString(lectureDto.startDateTime),
            end_time: DateConverter.toTimeString(lectureDto.endDateTime),
            eventId: lectureDto.eventId,
            lector: lectureDto.userId
        };
    }
}

export default Lecture;
