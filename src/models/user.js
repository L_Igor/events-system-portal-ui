class User {
    static convertFromVmToDto(userVm) {
        if (!userVm) {
            return {};
        }

        return {
            fullName: `${userVm.firstName} ${userVm.lastName}`,
            occupation: userVm.occupation,
            email: userVm.email,
            bio: userVm.bio
        };
    }

    static convertFromDtoToVm(userDto) {
        if (!userDto) {
            return {};
        }

        let [firstName, lastName] = userDto.fullName ? userDto.fullName.split(' ') : [];

        return {
            firstName: firstName,
            lastName: lastName,
            occupation: userDto.occupation,
            email: userDto.email,
            bio: userDto.bio
        };
    }
}

export default User;
