class DateConverter {
    static toDateString(date) {
        date = (date instanceof Date) ? date : new Date(date);

        const yyyy = date.getFullYear();
        const MM = this._addLeadingZero(date.getMonth() + 1);
        const dd = this._addLeadingZero(date.getDate());

        return `${yyyy}-${MM}-${dd}`;
    };

    static toTimeString(date) {
        date = (date instanceof Date) ? date : new Date(date);

        const HH = this._addLeadingZero(date.getHours());
        const mm = this._addLeadingZero(date.getMinutes());

        return `${HH}:${mm}`;
    }

    static toDateTimeString(date, time) {
        if (!(date && time)) {
            return null;
        }

        let timezoneOffset = new Date(date).getTimezoneOffset();
        const sing = -timezoneOffset >= 0 ? '+' : '-';
        timezoneOffset = Math.abs(timezoneOffset);
        const HH = this._addLeadingZero(timezoneOffset / 60);
        const mm = this._addLeadingZero(timezoneOffset % 60);

        return `${date}T${time}${sing}${HH}:${mm}`;
    }

    static _addLeadingZero(number) {
        return (number < 10) ? '0' + number : number;
    }
}

export default DateConverter;