import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
    static propTypes = {
        textTitle: PropTypes.string
    }

    static defaultProps = {
        textTitle: ''
    }


    render() {
        return (
            <header className="header">
                {this.props.children}
            </header>
        );
    }
}

export default Header;