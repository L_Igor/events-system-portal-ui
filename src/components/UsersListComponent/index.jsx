import React, { Component } from 'react';
import ItemTemplate from '../ItemTemplate';
import UserItem from '../UserItem';
import Header from '../Header';
import SearchComponent from '../SearchComponent';
import HeaderBtnBlockComponent from '../HeaderBtnBlockComponent';

class UsersListComponent extends Component {
    render() {
        const isSelected = false;

        return (
            <div>
                 <Header>
                    <button className="btn" onClick={this.props.newUser}>
                        Новый пользователь
                    </button>
                    <HeaderBtnBlockComponent visible={isSelected} />
                    <SearchComponent visible={!isSelected} initSearchString={this.props.searchString} searchHandler={this.props.userSearchHandler} />
                </Header>

                <main className="lectures main">
                    <h2 className="lectures__title title">Участники</h2>
                    {this.renderUsersList(this.props.usersList)}
                </main>
            </div>
        );
    }

    renderUsersList(data) {
        const { editClick, deleteClick, userClick, toggleUserClick } = this.props;

        return (
            <ul className="users__list list">
                {data.map(item =>
                    <ItemTemplate key={item.id} editHandler={() => editClick(item.id)} deleteHandler={() => deleteClick(item.id)}>
                        <UserItem user={item} userClick={() => userClick(item.id)} toggleUserClick={() => toggleUserClick(item.id)} isOpen={this.props.openUserId === item.id}/>
                    </ItemTemplate>
                )}
            </ul>
        );
    }
}

export default UsersListComponent;