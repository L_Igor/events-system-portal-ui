import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SearchComponent extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        initSearchString: PropTypes.string,
        searchHandler: PropTypes.func
    }

    constructor(props) {
        super(props);

        this.keyPressHandler = this.keyPressHandler.bind(this);
        this.clearSearchStringHandler = this.clearSearchStringHandler.bind(this);
        this.searchHandler = this.searchHandler.bind(this);
    }

    render() {
        const hidden = this.props.visible ? '' : 'hidden';

        return (
            <div className={`search ${hidden}`}>
                <input type="text" className="search__input input" placeholder="Поиск"
                       ref="searchString" defaultValue={this.props.initSearchString}
                       onKeyPress={this.keyPressHandler} />
                <button className="search__btn btn btn--search" onClick={this.searchHandler}>
                    <span className="glyphicon glyphicon-search"></span>
                    <span className="search__text visually-hidden">Поиск</span>
                </button>
                <button className="search__btn search__btn--close btn btn--icon btn--close" onClick={this.clearSearchStringHandler}>
                    <span className="btn__icon icon icon--close"></span>
                    <span className="search__text visually-hidden">Сброс</span>
                </button>
            </div>
        );
    }

    keyPressHandler(e) {
        e.key === 'Enter' && this.searchHandler();
    }

    clearSearchStringHandler() {
        this.refs.searchString.value = '';
        this.searchHandler();
    }

    searchHandler() {
        this.props.searchHandler(this.refs.searchString.value);
    }
}

export default SearchComponent;