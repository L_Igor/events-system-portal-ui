import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GravatarService from './CravatarServise';

class Gravatar extends Component {
    static propTypes = {
        size: PropTypes.string,
        user: PropTypes.shape({
            email: PropTypes.string,
            fullName: PropTypes.string,
            photo: PropTypes.string
        })
    }

    static defaultProps = {
        size: "small",
        user: {
            email: "",
            fullName: "",
            photo: ""
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            imgUrl: null,
            hasAvatar: false,
            literals: null,
            user: {
                email: null,
                fullName: null,
                photo: null
            }
        };
    }

    getAvatar() {
        const { user } = this.props;
        const userPhoto = user.photo;
        if (userPhoto && userPhoto.length > 1) {
            this.setState({
                hasAvatar: true,
                imgUrl: userPhoto
            });
        }

        if (!userPhoto || userPhoto.length < 1) {
            GravatarService.get(user.email)
            .then(function(response) {
                if (!response.ok) {
                    return Promise.reject();
                }
                return Promise.resolve(response.url);
            })
            .then(data => {
                this.setState({
                    hasAvatar: true,
                    imgUrl: data
                });
            })
            .catch(err => {
                let words = user.fullName.split(' ');
                let literals = (words.length > 1)
                    ? words[0][0] + words[1][0]
                    : words[0] + words[1];

                literals = literals.toUpperCase();

                this.setState({
                    hasAvatar: false,
                    literals: literals
                });
            });
        }
    }
    
    getContent() {
        if(this.state.hasAvatar) {
            return (
                <img className="gravatar__img" alt="" src={this.state.imgUrl} />
            );
        }

        return (
            <div className="gravatar__literals">
                <span className="gravatar__text">{this.state.literals}</span>
            </div>
        );
    }

    componentDidMount() {  
        this.getAvatar();
    }

    render() {
        return (
            <div className={`gravatar gravatar--${this.props.size}`}>
                {this.getContent()}
            </div>
        );
    }
}

export default Gravatar;
