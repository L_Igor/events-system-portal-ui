import * as md5 from 'md5';

class GravatarService {
    static endpoint = 'https://www.gravatar.com/avatar/';

    static get(email) {
        const emailHash = md5(email);
        const url = `${this.endpoint}${emailHash}`;
        return fetch(url, {method: "HEAD"});
    }

}

export default GravatarService;