import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import AuthService from '../services/AuthService';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirectToReferrer: false
        };

        this.onChnageHandler = this.onChnageHandler.bind(this);
        this.onSubmitHandler = this.onSubmitHandler.bind(this);

        this.authService = new AuthService();
    }

    componentWillMount() {
        if (this.authService.isAuthenticated()) {
            this.props.history.replace('/');
        }
    }

    render() {
        const { referrer } = this.props.location.state || { referrer: { pathname: '/' } };

        if (this.state.redirectToReferrer) {
            return (
                <Redirect to={referrer} />
            );
        }

        return (
            <div className="center row">
                <div className="card col-xs-6">
                    <form onSubmit={this.onSubmitHandler}>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="email" className="form-control" id="email" onChange={this.onChnageHandler} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="password">Пароль</label>
                            <input type="password" className="form-control" id="password" onChange={this.onChnageHandler} />
                        </div>

                        <div className="form-group">
                            <Link to={'/forget'}>Забыли пароль?</Link>
                        </div>

                        <div className="pull-right">
                            <button type="cancel" className="btn btn-default">Зарегистрироваться</button>
                            <button type="submit" className="btn btn-success">Войти</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }

    onChnageHandler(e) {
        this.setState(
            {
                [e.target.id]: e.target.value
            }
        );
    }

    onSubmitHandler(e) {
        e.preventDefault();

        this.authService.login(this.state.email, this.state.password)
            .then(res => {
                this.setState({ redirectToReferrer: true })
            })
            .catch(err => {
                alert(err);
            })
    }
}

export default Login;