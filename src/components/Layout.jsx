import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import NavMenu from './NavMenu';
import AuthService from '../services/AuthService';

import TokenStorage from '../services/TokenStorage';

// TODO: dependency injection
class Layout extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        children: PropTypes.object.isRequired
    }
    
    constructor(props) {
        super(props);

        this.authService = new AuthService();
    }

    render() {
        if (this.props.location.pathname === '/login') {
            return (
                this.props.children
            );
        }

        if (!this.authService.isAuthenticated()) {
            return (
                <div>
                    {this.props.children}
                    <Redirect to={{
                        pathname: '/login',
                        state: { referrer: this.props.location }
                    }} />
                </div>
            );
        }

        return (
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-sm-3'>
                        <NavMenu user={TokenStorage.getUser()} />
                    </div>
                    <div className='col-sm-9'>
                        {this.props.children}
                    </div>
                </div>
            </div>

        );
    }
}

export default withRouter(Layout);