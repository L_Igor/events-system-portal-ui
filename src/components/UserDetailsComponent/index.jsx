import React, { Component } from 'react';
import ItemTemplate from '../ItemTemplate';
import LectureItem from '../LectureItem';
import UserItem from '../UserItem';
import Header from '../Header';
import SearchComponent from '../SearchComponent';
import Gravatar from '../Gravatar';

class UserDetailsComponent extends Component {
    render() {
        const { user, newUserClick, editUserClick, deleteUserClick, gravatarInfo } = this.props;
        
        return (
            <div>
                 <Header>
                    <button className="btn" onClick={newUserClick}>
                        Новый пользователь
                    </button>
                    <SearchComponent />
                </Header>

                <main className="main">
                    <article className="user">
                        <h2 className="user__title title">Лектор</h2>
                        <section className="user__info list__item">
                            <Gravatar user={gravatarInfo} size='large'/>
                            <UserItem user={user} isOpen={true}/>
                            <div className="user__btn-container">
                                <button onClick={() => editUserClick(user.id)} className="user__btn list__btn btn btn--icon btn--edit">
                                    <span className="visually-hidden">Редактировать</span>
                                    <span className="glyphicon glyphicon-pencil"></span>
                                </button>
                                <button onClick={() => deleteUserClick(user.id)} className="user__btn btn  btn--icon btn--delete">
                                    <span className="visually-hidden">Удалить</span>
                                    <span className="glyphicon glyphicon-trash"></span>
                                </button>
                            </div>
                        </section>
                    </article>
                    <section className="user-lectures">
                        <h2 className="user__title title">Лекции</h2>
                        {this.renderUserLecturesList()}
                    </section>
                    
                </main>
            </div>
        );
    }

    renderUserLecturesList() {
        const { editLectureClick, deleteLectureClick, userClick = () => {} } = this.props;

        return(
            <ul className="user-lectures__list list">
                {this.props.userLectures.map(item =>
                    <ItemTemplate key={item.id} editHandler={() => editLectureClick(item.id)} deleteHandler={() => deleteLectureClick(item.id)}>
                        <LectureItem lecture={item} userClick={() => userClick(item.userId)}/>
                    </ItemTemplate>
                )}
            </ul>
        );
    }
}

export default UserDetailsComponent;