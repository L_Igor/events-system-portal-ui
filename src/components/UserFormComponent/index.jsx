import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { required, length, email, file } from 'redux-form-validators';

import { renderInputField, renderTextareaField, renderFileField } from '../RenderInputsComponent';

class UserFormComponent extends Component {

    render() {
        const { onSubmitHandler, onCancelHandler, pristine, submitting } = this.props;
        return (
            <form onSubmit={this.props.handleSubmit(onSubmitHandler)} className="form-user form">
                <Field
                    name="firstName"
                    type="text"
                    component={renderInputField}
                    label="Имя"
                    validate={[required(), length({ min: 3, max: 256 })]}
                />

                <Field
                    name="lastName"
                    type="text"
                    component={renderInputField}
                    label="Фамилия"
                    validate={[required(), length({ min: 3, max: 256 })]}
                />

                <Field
                    name="occupation"
                    type="text"
                    component={renderInputField}
                    label="Должность"
                    validate={[length({ max: 64 })]}
                />

                <Field
                    name="email"
                    type="email"
                    component={renderInputField}
                    label="Email"
                    validate={[required(), email()]}
                />

                <Field
                    name="bio"
                    component={renderTextareaField}
                    label="Краткое описание"
                    rows="8"
                    validate={[length({ max: 256 })]}
                />

                <Field
                    name="photo"
                    label="+ Добавить фото"
                    component={renderFileField}
                    validate={[file({ accept: 'image/*', maxSize: '1MB' })]}
                />

                <div className="form__buttons">
                    <button type="reset" className="form__btn btn btn--clear" onClick={onCancelHandler}>Отмена</button>
                    <button type="submit" className="form__btn btn" disabled={pristine || submitting}>Сохранить</button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'UserFormComponent',
    enableReinitialize: true
})(UserFormComponent);