import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Gravatar from '../Gravatar';

class UserCardComponent extends Component {
    static propTypes = {
        user: PropTypes.shape({
            id: PropTypes.number.isRequared,
            fullName: PropTypes.string,
            email: PropTypes.string,
            userName: PropTypes.string,
            bio: PropTypes.string,
            occupation: PropTypes.string,
            photo: PropTypes.string,
            statistics: PropTypes.string
        })
    }

    render() {
        const { user } = this.props;
        const gravatarInfo = {
            email: user.email,
            fullName: user.fullName,
            photo: user.photo
        };
        return (
            <section className="user-card" onClick={() => this.props.userClick(this.props.user.id)}>
                <Gravatar user={gravatarInfo} size='small'/>
                <a className="user-card__name link">{user.fullName}</a>
            </section>
        );
    }
}
export default UserCardComponent;