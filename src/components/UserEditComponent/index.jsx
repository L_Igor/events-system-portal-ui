import React, { Component } from 'react';

import Header from '../Header';
import UserFormComponent from '../UserFormComponent';

class UserEditComponent extends Component {
    constructor(props) {
        super(props);
        this.titleText = 'Редактирование'
    }

    render() {
        return (
            <div className="content">
                <Header>
                    <h1 className="header__title title">{this.titleText}</h1>
                </Header>
                <UserFormComponent
                    initialValues={this.props.user}
                    onSubmitHandler={this.props.updateUser}
                    onCancelHandler={this.props.cancel}
                />
            </div>
        );
    }
}

export default UserEditComponent;