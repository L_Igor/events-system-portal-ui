import React, { Component } from 'react';

class NotFound extends Component {
    render() {
        return (
            <div>Ресурс не найден</div>
        );
    }
}

export default NotFound;