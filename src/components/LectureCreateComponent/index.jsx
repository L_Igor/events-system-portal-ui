import React, { Component } from 'react';

import Header from '../Header';
import LectureFormComponent from '../LectureFormComponent';

class LectureCreateComponent extends Component {
    constructor(props) {
        super(props);
        this.titleText = 'Новая лекция'
    }

    render() {
        return (
            <div className="content">
                <Header>
                    <h1 className="header__title title">{this.titleText}</h1>
                </Header>
                <LectureFormComponent
                    users={this.props.usersList}
                    onSubmitHandler={this.props.addLecture}
                    onCancelHandler={this.props.cancel}
                />
            </div>
        );
    }
}

export default LectureCreateComponent;