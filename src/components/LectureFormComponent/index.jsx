import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { required, length, date } from 'redux-form-validators';

import { renderInputField, renderTextareaField, renderSelectField } from '../RenderInputsComponent';

class LectureFormComponent extends Component {
    render() {
        const { onSubmitHandler, onCancelHandler, pristine, submitting } = this.props;
        return (
            <form onSubmit={this.props.handleSubmit(onSubmitHandler)} className="form-lecture form">
                <Field
                    name="name"
                    type="text"
                    component={renderTextareaField}
                    label="Название"
                    validate={[required(), length({ min: 3, max: 256 })]}
                />

                <Field
                    name="description"
                    component={renderTextareaField}
                    label="Краткое описание"
                    validate={[length({ max: 256 })]}
                />

                <div className="form__date-lecture">

                    <Field
                        name="date"
                        type="date"
                        component={renderInputField}
                        label="Дата"
                        validate={[required(), date({ format: 'yyyy-mm-dd', '>=': 'today' })]}
                    />

                    <Field
                        name="start_time"
                        type="time"
                        component={renderInputField}
                        label="Начало"
                        validate={[required()]}
                    />

                    <Field
                        name="end_time"
                        type="time"
                        component={renderInputField}
                        label="Конец"
                    />

                </div>


                <Field
                    name="lector"
                    component={renderSelectField}
                    label="Лектор"
                    validate={[required()]}
                >
                    {this.props.users.map((user) => {
                        return (
                            <option value={user.id} className="form__option" key={user.id}>{user.fullName}</option>
                        );
                    })}
                </Field>

                <div className="form__buttons">
                    <button type="reset" className="form__btn btn btn--clear" onClick={onCancelHandler}>Отмена</button>
                    <button type="submit" className="form__btn btn" disabled={pristine || submitting}>Сохранить</button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'LectureFormComponent',
    enableReinitialize: true
})(LectureFormComponent);