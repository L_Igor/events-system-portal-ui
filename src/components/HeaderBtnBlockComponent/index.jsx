import React, { Component } from 'react';
import PropTypes from 'prop-types';

class HeaderBtnBlockComponent extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        selectAllHandler: PropTypes.func,
        deleteHandler: PropTypes.func,
        cancelHandler: PropTypes.func
    }

    render() {
        const hidden = this.props.visible ? '' : 'hidden';

        return (
            <div className={`header__btn-container ${hidden}`}>
                <button onClick={this.props.selectAllHandler} className="header__btn btn btn--icon btn--select-all">
                    <span className="glyphicon glyphicon-unchecked"></span>
                    <span className="btn__name">Выбрать все</span>
                </button>
                <button onClick={this.props.deleteHandler} className="header__btn btn  btn--icon btn--delete">
                    <span className="glyphicon glyphicon-trash"></span>
                    <span className="btn__name">Удалить</span>
                </button>
                <button onClick={this.props.cancelHandler} className="header__btn btn  btn--icon btn--cancel">
                    <span className="glyphicon glyphicon-remove-sign"></span>
                    <span className="btn__name">Отмена</span>
                </button>
            </div>
        );
    }
}

export default HeaderBtnBlockComponent;