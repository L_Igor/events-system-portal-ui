import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LectureTime extends Component {
    static propTypes = {
        time: PropTypes.shape({
            start: PropTypes.string,
            end: PropTypes.string
        })
    }

    constructor(props) {
        super(props);
        this.state = {
            time: ''
        };
    }

    getTime(time) {
        let timeFull = new Date(time),
            hours = (+timeFull.getHours()) < 10 ? '0' + timeFull.getHours() : timeFull.getHours(),
            minutes = (+timeFull.getMinutes()) < 10 ? '0' + timeFull.getMinutes() : timeFull.getMinutes();
        return hours + ':' + minutes;
    }


    transformTime(){
        let timeStartValue = this.getTime(this.props.time.start),
            timeEndValue = this.getTime(this.props.time.end);
        let timeValue = timeStartValue + ' - ' + timeEndValue;
        this.setState({
            time: timeValue
        });
    }

    componentDidMount() {
        this.transformTime();
    }

    render() {
        return (
            <div className="time">
                <span className="glyphicon glyphicon-time"></span>
                <span className="time__value">{this.state.time}</span>
            </div>
        );
    }
}

export default LectureTime;