import React, { Component } from 'react';

import Header from '../Header';
import LectureFormComponent from '../LectureFormComponent';

class LectureEditComponent extends Component {
    constructor(props) {
        super(props);
        this.titleText = 'Редактирование'
    }

    render() {
        return (
            <div className="content">
                <Header>
                    <h1 className="header__title title">{this.titleText}</h1>
                </Header>
                <LectureFormComponent
                    initialValues={this.props.lecture}
                    users={this.props.usersList}
                    onSubmitHandler={this.props.updateLecture}
                    onCancelHandler={this.props.cancel}
                />
            </div>
        );
    }
}

export default LectureEditComponent;