import React, { Component } from 'react';
import ItemTemplate from '../ItemTemplate';
import LectureItem from '../LectureItem';
import Header from '../Header';
import SearchComponent from '../SearchComponent';
import HeaderBtnBlockComponent from '../HeaderBtnBlockComponent';

class LecturesListComponent extends Component {
    render() {
        const { lecturesList, newLectureClick } = this.props;
        const isSelected = false;

        return (
            <div className="content-container">
                <Header>
                    <button className="btn" onClick={newLectureClick}>
                        Новaя лекция
                    </button>
                    <HeaderBtnBlockComponent visible={isSelected} />
                    <SearchComponent visible={!isSelected} initSearchString={this.props.searchString} searchHandler={this.props.lectureSearchHandler} />
                </Header>

                <main className="lectures main">
                    <h2 className="lectures__title title">Список лекций</h2>
                    {this.renderLectureList(lecturesList)}
                </main>

            </div>
        );
    }

    renderLectureList(data) {
        const { editClick, deleteClick, userClick, toggleLectureClick } = this.props;

        return (
            <ul className="lectures__list list">
                {data.map(item =>
                    <ItemTemplate key={item.id} editHandler={() => editClick(item.id)} deleteHandler={() => deleteClick(item.id)}>
                        <LectureItem lecture={item} userClick={() => userClick(item.userId)} toggleLectureClick={() => toggleLectureClick(item.id)} isOpen={this.props.openLectureId === item.id} />
                    </ItemTemplate>
                )}
            </ul>
        );
    }
}

export default LecturesListComponent;