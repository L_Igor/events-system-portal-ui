import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

class NavMenu extends Component {
    static propTypes = {
        user: PropTypes.shape({
            name: PropTypes.string,
            email: PropTypes.string
        })
    }

    static defaultProps = {
        user: {
            name: '',
            email: ''
        }
    }

    render() {
        return (
            <div className='main-nav'>
                <div className='navbar navbar-inverse'>
                    <div className='navbar-header'>
                        <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                            <span className='sr-only'>Toggle navigation</span>
                            <span className='icon-bar'></span>
                            <span className='icon-bar'></span>
                            <span className='icon-bar'></span>
                        </button>
                    </div>
                    <div className="main-nav__user">
                        <p className="main-nav__user-name">{this.props.user.name}</p>
                        <p className="main-nav__user-email">{this.props.user.email}</p>
                    </div>
                    <nav className='navbar-collapse collapse'>
                        <ul className='nav navbar-nav main-nav__list list'>
                            <li className='nav__item'>
                                <NavLink to={'/users'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-user'></span>
                                    <span className='main-nav__text'>Пользователи</span>
                                </NavLink>
                            </li>
                            <li className='nav__item'>
                                <NavLink to={'/lectures'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-file'></span>
                                    <span className='main-nav__text'>Лекции</span>
                                </NavLink>
                            </li>
                            <li className='nav__item'>
                                <NavLink to={'/settings'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-cog'></span>
                                    <span className='main-nav__text'>Настройки</span>
                                </NavLink>
                            </li>
                            <li className='nav__item'>
                                <NavLink to={'/help'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-question-sign'></span>
                                    <span className='main-nav__text'>Помощь</span>
                                </NavLink>
                            </li>
                            <li className='nav__item nav__item--out'>
                                <NavLink to={'/logout'} activeClassName='active'>
                                    <span className='glyphicon glyphicon-log-out'></span>
                                    <span className='main-nav__text'>Выход</span>
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        );
    }
}

export default NavMenu;
