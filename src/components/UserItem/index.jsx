import React, { Component } from 'react';
import PropTypes from 'prop-types';

class UserItem extends Component {
    static propTypes = {
        userClick: PropTypes.func,
        isOpen: PropTypes.bool,
        user: PropTypes.shape({
            id: PropTypes.number.isRequared,
            fullName: PropTypes.string,
            email: PropTypes.string,
            userName: PropTypes.string,
            bio: PropTypes.string,
            occupation: PropTypes.string,
            photo: PropTypes.string,
            statistics: PropTypes.string
        })
    }

    static defaultProps = {
        isOpen: false
    }

    render() {
        return (
            <section className="user-item" onClick={this.props.toggleUserClick}>
                <div className="user-item__contacts">
                    <h3 className="user-item__name title title--item">
                        <a className="link link--title" onClick={this.props.userClick}>{this.props.user.fullName}</a>
                    </h3>
                    <a className="user-item__email link" href="mailto:{this.props.user.email}">{this.props.user.email}</a>
                </div>
                {this.renderBio()}
            </section>
        );
    }

    renderBio() {
        if(this.props.isOpen){
            return(
                <div className="user-item__info">
                    <p className="user-item__text">{this.props.user.bio}</p>
                </div>
            );
        }
    }
}

export default UserItem;