import React, { Component } from 'react';
import PropTypes from 'prop-types';

class PopUpComponent extends Component {
    static propTypes = {
        text: PropTypes.shape({
            question: PropTypes.string,
            notice: PropTypes.string
        }),
        submit: PropTypes.func,
        cancel: PropTypes.func
    }

    componentDidMount() {
        
    }

    render() {
        return (
            <div className="container-veil">
                {this.getContent()}
            </div>
        );
    }

    getContent() {
        return(
            <div className="pop-up">
                <p className="pop-up__text">{this.props.text.question}</p>
                <div className="pop-up__btn-container">
                    <button className="pop-up__btn btn btn--clear">Отмена</button>
                    <button className="pop-up__btn btn">Удалить</button>
                </div>
            </div>
        );
    }

    changeContent() {
        return(
            <div className="pop-up">
                <span className="glyphicon glyphicon-ok-circle"></span>
                <p className="pop-up__text">{this.props.text.notice}</p>
            </div>
        );
    }
}

export default PopUpComponent;