import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    deleteHandler: PropTypes.func,
    editHandler: PropTypes.func,
    children: PropTypes.element.isRequired
};

class ItemTemplate extends Component {
    render() {
        return (
            <li className="list__item">
                <label className="list__label checkbox">
                    <input className="checkbox__input" type="checkbox" />
                    <span className="checkbox__input-custom"></span>
                </label>
                <div className="list__content">{this.props.children}</div>
                <div className="list__btn-container">
                    <button onClick={this.props.editHandler} className="list__btn btn btn--icon btn--edit">
                        <span className="visually-hidden">Редактировать</span>
                        <span className="glyphicon glyphicon-pencil"></span>
                    </button>
                    <button onClick={this.props.deleteHandler} className="list__btn btn  btn--icon btn--delete">
                        <span className="visually-hidden">Удалить</span>
                        <span className="glyphicon glyphicon-trash"></span>
                    </button>
                </div>
            </li>
        );
    }
}

ItemTemplate.propTypes = propTypes;

export default ItemTemplate;