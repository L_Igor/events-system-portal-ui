import React, { Component } from 'react';
import PropTypes from 'prop-types';

class LectureDate extends Component {
    static propTypes = {
        date: PropTypes.string
    }

    constructor(props) {
        super(props);
        this.state = {
            date: ''
        };
    }

    transformDate(){
        let date = new Date(this.props.date),
            day = (+date.getDate()) < 10 ? '0' + date.getDate() : date.getDate(),
            month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);

        let dateValue = day + '.' + month + '.' + date.getFullYear();
        this.setState({
            date: dateValue
        });
    }

    componentDidMount() {
        this.transformDate();
    }

    render() {
        return (
            <div className="date">
                <span className="glyphicon glyphicon-calendar"></span>
                <span className="date__value">{this.state.date}</span>
            </div>
        );
    }
}

export default LectureDate;