import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import TokenStorage from '../services/TokenStorage';

class Logout extends Component {
    render() {
        TokenStorage.removeToken();

        return (
            <Redirect to={'/'} />
        );
    }
}

export default Logout;