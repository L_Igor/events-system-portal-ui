import React from 'react';

const renderInputField = function ({ input, label, type, meta: { touched, error } = {}, ...inputProps }) {
    inputProps = { ...input, ...inputProps };
    let feedback = touched && error;
    let invalid = !!(touched && error) ? 'is-invalid' : '';
    let hasError = !!(touched && error) ? 'has-error' : '';
    let htmlId = 'id-' + inputProps.name;

    return (
        <label htmlFor={htmlId} className={`form__label ${hasError}`}>
            <p className="form__name">{label}</p>
            <input className={`form__input input ${invalid}`} type={type || 'text'} id={htmlId} {...inputProps} />
            {feedback ? <span className="help-block">{feedback}</span> : ''}
        </label>
    )
};

const renderTextareaField = function ({ input, label, meta: { touched, error } = {}, ...inputProps }) {
    inputProps = { ...input, ...inputProps };
    let feedback = touched && error;
    let invalid = !!(touched && error) ? 'is-invalid' : '';
    let hasError = !!(touched && error) ? 'has-error' : '';
    let htmlId = 'id-' + inputProps.name;

    return (
        <label htmlFor={htmlId} className={`form__label ${hasError}`}>
            <p className="form__name">{label}</p>
            <textarea className={`form__textarea textarea ${invalid}`} id={htmlId} {...inputProps}></textarea>
            {feedback ? <span className="help-block">{feedback}</span> : ''}
        </label>
    )
};

const renderSelectField = function ({ input, label, children, meta: { touched, error } = {}, ...inputProps }) {
    inputProps = { ...input, ...inputProps };
    let feedback = touched && error;
    let invalid = !!(touched && error) ? 'is-invalid' : '';
    let hasError = !!(touched && error) ? 'has-error' : '';
    let htmlId = 'id-' + inputProps.name;

    return (
        <label htmlFor={htmlId} className={`form__label ${hasError}`}>
            <p className="form__name">{label}</p>
            <select {...input} id={htmlId} className={`form__select input ${invalid}`}>
                {children}
            </select>
            {feedback ? <span className="help-block">{feedback}</span> : ''}
        </label>
    )
};

const renderFileField = function ({ input, label, type, meta: { touched, error } = {}, ...inputProps }) {
    inputProps = { ...input, ...inputProps };
    delete inputProps.value;
    let feedback = touched && error;
    let invalid = !!(touched && error) ? 'is-invalid' : '';
    let hasError = !!(touched && error) ? 'has-error' : '';
    let htmlId = 'id-' + inputProps.name;

    return (
        <label htmlFor={htmlId} className={`form__label ${hasError}`}>
            <p className="form__file">{label}</p>
            <input type="file" id={htmlId} {...inputProps} style={{display: "none"}} className={`form__input input ${invalid}`}/>
            {feedback ? <span className="help-block">{feedback}</span> : ''}
        </label>
    )
}

export {renderInputField, renderTextareaField, renderSelectField, renderFileField};