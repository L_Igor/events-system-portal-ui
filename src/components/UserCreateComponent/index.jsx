import React, { Component } from 'react';

import Header from '../Header';
import UserFormComponent from './../UserFormComponent/index';

class UserCreateComponent extends Component {
    constructor(props) {
        super(props);
        this.titleText = 'Новый пользователь'
    }

    render() {
        return (
            <div className="content">
                <Header>
                    <h1 className="header__title title">{this.titleText}</h1>
                </Header>
                <UserFormComponent
                    onSubmitHandler={this.props.addUser}
                    onCancelHandler={this.props.cancel}
                />
            </div>
        );
    }
}

export default UserCreateComponent;