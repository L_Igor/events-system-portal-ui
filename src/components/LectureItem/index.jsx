import React, { Component } from 'react';
import PropTypes from 'prop-types';

import LectureDate from '../LectureDate';
import LectureTime from '../LectureTime';
import UserCardContainer from '../../containers/UserCardContainer';

class LectureItem extends Component {
    static propTypes = {
        userClick: PropTypes.func,
        lectureClick: PropTypes.func,
        isOpen: PropTypes.bool,
        lecture: PropTypes.shape({
            id: PropTypes.number.isRequared,
            name: PropTypes.string,
            description: PropTypes.string,
            startDateTime: PropTypes.string,
            endDateTime: PropTypes.string,
            rating: PropTypes.number,
            countRatings: PropTypes.integer,
            countQuestions: PropTypes.integer,
            eventId: PropTypes.integer,
            userId: PropTypes.integer
        })
    }

    static defaultProps = {
        isOpen: false,
        lecture: {
            id: 0,
            name: '',
            description: '',
            startDateTime: '',
            endDateTime: '',
            rating: null,
            countRatings: null,
            countQuestions: null,
            eventId: null,
            userId: null
        }
    }


    render() {
        const lectureTime = {
            start: this.props.lecture.startDateTime,
            end: this.props.lecture.endDateTime
        }
        return (
            <section className="lecture" onClick={this.props.toggleLectureClick}>
                <div className="lecture__description">
                    <h3 className="title title--item">
                        <a className="link link--title">{this.props.lecture.name}</a>
                    </h3>
                    {this.renderLectureDesc()}
                </div>
                <div className="lecture__info" >
                    <UserCardContainer userId={this.props.lecture.userId} userClick={this.props.userClick}/>
                    {this.renderLectureDate()}
                    {this.renderLectureTime(lectureTime)}
                </div>
            </section>
        );
    }

    renderLectureDesc(){
        if(this.props.isOpen){
            return <p className="lecture__text">{this.props.lecture.description}</p>
        }
    }

    renderLectureDate(){
        if(this.props.isOpen){
            return <LectureDate date={this.props.lecture.startDateTime}/>
        }
    }

    renderLectureTime(lectureTime){
        if(this.props.isOpen){
            return <LectureTime time={lectureTime}/>
        }
    }
}

export default LectureItem;