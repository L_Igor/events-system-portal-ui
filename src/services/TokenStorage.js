import decode from 'jwt-decode';

class TokenStorage {
    static key = 'token';

    static setToken(token) {
        localStorage.setItem(this.key, JSON.stringify(token));
    }

    static getToken() {
        return JSON.parse(localStorage.getItem(this.key));
    }

    static getAccessToken() {
        const token = this.getToken();
        return token && token.access_token;
    }

    static getRefreshToken() {
        const token = this.getToken();
        return token && token.refresh_token;
    }

    static removeToken() {
        localStorage.removeItem(this.key);
    }

    static isTokenExpired(token) {
        try {
            const decoded = decode(token);
            return decoded.exp < Date.now() / 1000;
        }
        catch (err) {
            return true;
        }
    }

    static getUser() {
        try {
            const token = this.getToken();
            const accessToken = !!token && token.access_token;
            const decoded = decode(accessToken);
            const user = {
                name: decoded['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'],
                email: decoded['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name']
            };
            return user;
        }
        catch (err) {
            console.log(err);
        }
    }
}

export default TokenStorage;
