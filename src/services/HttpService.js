import { Environment } from '../environment/environment';
import TokenStorage from './TokenStorage';
import AuthService from './AuthService';

class HttpService {
    constructor(apiEndpoint) {
        this.apiEndpoint = apiEndpoint || Environment.ApiEndpoint;

        this.authService = new AuthService(this.apiEndpoint);
    }

    get(url) {
        const config = {
            method: 'GET',
        };

        return this._executeRequest(url, config);
    }

    post(url, data) {
        const config = {
            method: 'POST',
            body: JSON.stringify(data)
        };

        return this._executeRequest(url, config);
    }

    put(url, data) {
        const config = {
            method: 'PUT',
            body: JSON.stringify(data)
        };

        return this._executeRequest(url, config);
    }

    delete(url) {
        const config = {
            method: 'DELETE',
        };

        return this._executeRequest(url, config);
    }

    uploadFile(url, data) {
        const config = {
            method: 'POST',
            body: data
        };

        return this._executeRequest(url, config);
    }

    _executeRequest(url, config) {
        const fullUrl = `${this.apiEndpoint}/${url}`.replace(/\/+/g, '/').replace(/:\//, '://');
        config.headers = {...this.getHeaders(), ...config.headers};
        if (config.body instanceof FormData) {
            delete config.headers['Content-Type'];
        }
        const request = new Request(fullUrl, config);

        const checkAuthorizationStatus = this._checkAuthorizationStatus.bind(this, request.clone());

        return fetch(request)
            .then(checkAuthorizationStatus)
            .then(this._checkStatus)
            .then(this._convertResponseToJson)
            .then(data => {
                return data;
            })
            .catch(e => {
                console.log(e);
                // Promise.reject(e);
            });
    }

    getHeaders() {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (!!TokenStorage.getToken()) {
            headers['Authorization'] = `Bearer ${TokenStorage.getAccessToken()}`;
        }

        return headers;
    }

    _checkAuthorizationStatus(request, response) {
        // console.log('_checkAuthorizationStatus');

        if (response.status === 401) {
            return ( // ????
                this.authService.updateAccessToken()
                    .then(res => {
                        request.headers.set('Authorization', `Bearer ${TokenStorage.getAccessToken()}`);

                        return fetch(request);
                    }, err => {
                        console.log('redirecting on login page ...', err);
                    })
                // .catch(err => {
                //     console.log(err);
                //     Promise.reject(err);
                // })
            );
        }

        return response;
    }

    _checkStatus(response) {
        // console.log('_checkStatus');
        // console.log(response);
        if (response.ok) {
            return Promise.resolve(response);
        }  else {
            const error = new Error(response.statusText);
            error.response = response;
            throw error;
        }
    }

    _convertResponseToJson(response) {
        // console.log('_convertResponseToJson');
        const contentType = response.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
            return response.json();
        }
        throw new TypeError("Oops, we haven't got JSON!");
    }
}

export default HttpService;