import { Environment } from '../environment/environment';
import TokenStorage from './TokenStorage';

class AuthService {
    constructor(apiEndpoint) {
        this.apiEndpoint = apiEndpoint || Environment.ApiEndpoint;

        this.fetch = this.fetch.bind(this);
    }

    login(username, password) {
        return this.receiveAccessToken(username, password);
    }

    logout() {
        TokenStorage.removeToken();
    }

    receiveAccessToken(username, password) {
        const data = {
            grant_type: 'password',
            username: username,
            password: password
        };

        return this.receiveToken(data);
    }

    updateAccessToken() {
        const refreshToken = TokenStorage.getRefreshToken();
        const data = {
            grant_type: 'refresh_token',
            refresh_token: refreshToken
        };

        return this.receiveToken(data);
    }

    receiveToken(data) {
        return this.fetch(`${this.apiEndpoint}/token`.replace(/\/+/g, '/').replace(/:\//, '://'), {
            method: 'POST',
            body: JSON.stringify(data)
        })
            .then(res => {
                TokenStorage.setToken(res);
                return Promise.resolve(res);
            })
            .catch(err => {
                console.log(err);
            });
    }

    loggedIn() {
        const token = TokenStorage.getToken();
        return !!token && !TokenStorage.isTokenExpired(token.access_token);
    }

    isAuthenticated() {
        const token = TokenStorage.getToken();
        return !!token;
    }

    // async isAuthenticated() {
    //     let isAuthenticated = this.loggedIn();

    //     if (!isAuthenticated) {
    //         await this.updateAccessToken();
    //         isAuthenticated = this.loggedIn();
    //     }

    //     return isAuthenticated;
    // }


    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    _checkStatus(response) {
        if (response.ok) {
            return response;
        } else {
            var error = new Error(response.statusText);
            error.response = response;
            throw error;
        }
    }
}

export default AuthService;
