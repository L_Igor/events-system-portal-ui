import * as types from './actionTypes';
import HttpService from '../../services/HttpService';
import User from '../../models/user';

const httpService = new HttpService();

export function getUsers() {
    return (dispatch, getState) => {
            httpService.get('/users')
            .then(data => {
                dispatch({ type: types.USERS_GET, payload: data });
            });
    };
}

const uploadAvatar = function (userId, file) {
    const formData = new FormData();
    formData.append('file', file);

    return httpService.uploadFile('/storage', formData)
        .then(data => {
            const photo = {
                photoId: data[0].fileId
            };

            return httpService.put(`/users/${userId}/avatar`, photo)
        });
}

export function addUser(user, file) {
    return (dispatch, getState) => {
        user = User.convertFromVmToDto(user);

        httpService.post('/users', user)
            .then(data => {
                if (file) {
                    const userId = data.id;
                    uploadAvatar(userId, file)
                        .then(data => {
                            // dispatch({ type: types.USER_ADD, payload: data });
                        });
                }

                dispatch({ type: types.USER_ADD, payload: data });
            });
    };
}

export function updateUser(id, user, file) {
    return (dispatch, getState) => {
        user = User.convertFromVmToDto(user);

        httpService.put(`/users/${id}`, user)
            .then(data => {
                if (file) {
                    const userId = data.id;
                    uploadAvatar(userId, file)
                        .then(data => {
                            // dispatch({ type: types.USER_UPDATE, payload: data });
                        });
                }

                dispatch({ type: types.USER_UPDATE, payload: data });
            });
    };
}

export function getUser(id) {
    return (dispatch, getState) => {
        httpService.get(`/users/${id}`)
            .then(data => {
                dispatch({ type: types.USER_GET, payload: data });
            });
    };
}

export function getUserLectures(id) {
    return (dispatch, getState) => {
        httpService.get(`/users/${id}/lectures`)
            .then(data => {
                dispatch({ type: types.USER_LECTURES_GET, payload: data });
            });
    };
}

export function deleteUser(id) {
    return (dispatch, getState) => {
        httpService.delete(`/users/${id}`)
            .then(data => {
                dispatch({ type: types.USER_DELETE, payload: id });
            });
    };
}

export function openUser(id) {
    return (dispatch, getState) => {
        dispatch({ type: types.USER_UI_OPEN, payload: id });
    };
}

export function closeUser() {
    return (dispatch, getState) => {
        dispatch({ type: types.USER_UI_CLOSE });
    };
}

export function searchUser(searchString) {
    return (dispatch, getState) => {
        dispatch({ type: types.SEARCH_USER, payload: searchString });
    };
}
