import * as types from './actionTypes';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
    usersList: [],
    activeUser: null,
    userLectures: [],
    searchTerm: '',
    filteredUsersList: undefined,
});

export default function reduce(state = initialState, action = {}) {
    const payload = action.payload;

    switch (action.type) {
        case types.USERS_GET:
            return state.merge({
                usersList: payload
            });
        case types.USER_ADD:
            return state.merge({
                usersList: [...state.usersList, payload]
            });
        case types.USER_UPDATE:
            return state.merge({
                usersList: state.usersList.map(item => item.id !== payload.id ? item : payload)
            });
        case types.USER_GET:
            return state.merge({
                activeUser: payload
            });
        case types.USER_LECTURES_GET:
            return state.merge({
                userLectures: payload
            });
        case types.USER_DELETE:
            return state.merge({
                usersList: state.usersList.filter(item => item.id !== payload)
            });
        case types.USER_UI_OPEN:
            return state.merge({
                openUserId: payload
            });
        case types.USER_UI_CLOSE:
            return state.merge({
                openUserId: null
            });
        case types.SEARCH_USER:
            const value = payload.trim().toLowerCase();
            const predicate = item => {
                return item.fullName.toLowerCase().includes(value)
                    || item.email.toLowerCase().includes(value);
            };
            const filteredData = value ? state.usersList.filter(predicate) : undefined;

            return state.merge({
                searchTerm: payload,
                filteredUsersList: filteredData
            });
        default:
            return state;
    }
}


// selectors
export function getUsersList(state) {
    return state.users.filteredUsersList || state.users.usersList;
}

export function getUserLectures(state) {
    return state.users.userLectures;
}

export function getActiveUser(state) {
    return state.users.activeUser;
}

export function getOpenUserId(state) {
    return state.users.openUserId;
}

export function getUserById(state, id) {
    return state.users.usersList.find((item) => {
        return item.id === id;
    });
}

export function getSearchTerm(state) {
    return state.users.searchTerm;
}
