export const USERS_GET = 'users.USERS_GET';
export const USER_ADD = 'users.USER_ADD';
export const USER_UPDATE = 'users.USER_UPDATE';
export const USER_GET = 'users.USER_GET';
export const USER_DELETE = 'users.USER_DELETE';
export const USER_LECTURES_GET = 'users.USER_LECTURES_GET';

export const USER_UI_OPEN = 'users.USER_UI_OPEN';
export const USER_UI_CLOSE = 'users.USER_UI_CLOSE';

export const SEARCH_USER = 'users.SEARCH_USER';