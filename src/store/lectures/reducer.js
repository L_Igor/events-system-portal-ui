import * as types from './actionTypes';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
    lecturesList: [],
    activeLecture: null,
    openLectureId: null,
    searchTerm: '',
    filteredLecturesList: undefined,
});

export default function reduce(state = initialState, action = {}) {
    const payload = action.payload;

    switch (action.type) {
        case types.LECTURES_GET:
            return state.merge({
                lecturesList: payload
            });
        case types.LECTURE_ADD:
            return state.merge({
                lecturesList: [...state.lecturesList, payload]
            });
        case types.LECTURE_UPDATE:
            return state.merge({
                lecturesList: state.lecturesList.map(item => item.id !== payload.id ? item : payload)
            });
        case types.LECTURE_GET:
            return state.merge({
                activeLecture: payload
            });
        case types.LECTURE_DELETE:
            return state.merge({
                lecturesList: state.lecturesList.filter(item => item.id !== payload)
            });
        case types.LECTURE_UI_OPEN:
            return state.merge({
                openLectureId: payload
            });
        case types.LECTURE_UI_CLOSE:
            return state.merge({
                openLectureId: null
            });
        case types.SEARCH_LECTURE:
            const value = payload.trim().toLowerCase();
            const predicate = item => {
                return item.name.toLowerCase().includes(value)
                    || item.description.toLowerCase().includes(value);
            };
            const filteredData = value ? state.lecturesList.filter(predicate) : undefined;

            return state.merge({
                searchTerm: payload,
                filteredLecturesList: filteredData
            });
        default:
            return state;
    }
}


// selectors
export function getLecturesList(state) {
    return state.lectures.filteredLecturesList || state.lectures.lecturesList;
}

export function getActiveLecture(state) {
    return state.lectures.activeLecture;
}

export function getOpenLectureId(state) {
    return state.lectures.openLectureId;
}

export function getSearchTerm(state) {
    return state.lectures.searchTerm;
}
