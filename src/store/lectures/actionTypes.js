export const LECTURES_GET = 'lectures.LECTURES_GET';
export const LECTURE_ADD = 'lectures.LECTURES_ADD';
export const LECTURE_UPDATE = 'lectures.LECTURE_UPDATE';
export const LECTURE_GET = 'lectures.LECTURE_GET';
export const LECTURE_DELETE = 'lectures.LECTURE_DELETE';

export const LECTURE_UI_OPEN = 'lectures.LECTURE_UI_OPEN';
export const LECTURE_UI_CLOSE = 'lectures.LECTURE_UI_CLOSE';

export const SEARCH_LECTURE = 'lectures.SEARCH_LECTURE';