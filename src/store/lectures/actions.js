import * as types from './actionTypes';
import HttpService from '../../services/HttpService';
import Lecture from '../../models/lecture';

const httpService = new HttpService();

export function getLectures() {
    return (dispatch, getState) => {
        httpService.get('/lectures')
            .then(data => {
                dispatch({ type: types.LECTURES_GET, payload: data });
            });
    };
}

export function addLecture(lecture) {
    return (dispatch, getState) => {
        lecture = Lecture.convertFromVmToDto(lecture);

        httpService.post('/lectures', lecture)
            .then(data => {
                dispatch({ type: types.LECTURE_ADD, payload: data });
            });
    };
}

export function updateLecture(id, lecture) {
    return (dispatch, getState) => {
        lecture = Lecture.convertFromVmToDto(lecture);

        httpService.put(`/lectures/${id}`, lecture)
            .then(data => {
                dispatch({ type: types.LECTURE_UPDATE, payload: data });
            });
    };
}

export function getLecture(id) {
    return (dispatch, getState) => {
        httpService.get(`/lectures/${id}`)
            .then(data => {
                dispatch({ type: types.LECTURE_GET, payload: data });
            });
    };
}

export function deleteLecture(id) {
    return (dispatch, getState) => {
        httpService.delete(`/lectures/${id}`)
            .then(data => {
                dispatch({ type: types.LECTURE_DELETE, payload: id });
            });
    };
}

export function openLecture(id) {
    return (dispatch, getState) => {
        dispatch({ type: types.LECTURE_UI_OPEN, payload: id });
    };
}

export function closeLecture() {
    return (dispatch, getState) => {
        dispatch({ type: types.LECTURE_UI_CLOSE });
    };
}

export function searchLecture(searchString) {
    return (dispatch, getState) => {
        dispatch({ type: types.SEARCH_LECTURE, payload: searchString });
    };
}
