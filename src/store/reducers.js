import users from './users/reducer';
import lectures from './lectures/reducer';
import { reducer as form } from 'redux-form';

export {
    form,
    users,
    lectures
};