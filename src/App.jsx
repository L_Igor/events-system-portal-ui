import React, { Component } from 'react';

import * as RoutesModule from './routes';
let routes = RoutesModule.routes;

class App extends Component {
  render() {
    return (
      <div>
        {routes}
      </div>
    );
  }
}

export default App;
